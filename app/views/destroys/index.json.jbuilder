json.array!(@destroys) do |destroy|
  json.extract! destroy, :id, :title, :post_id
  json.url destroy_url(destroy, format: :json)
end
