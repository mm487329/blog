json.array!(@updates) do |update|
  json.extract! update, :id, :title, :post_id
  json.url update_url(update, format: :json)
end
