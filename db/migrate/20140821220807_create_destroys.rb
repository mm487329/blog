class CreateDestroys < ActiveRecord::Migration
  def change
    create_table :destroys do |t|
      t.string :title
      t.integer :post_id

      t.timestamps
    end
  end
end
