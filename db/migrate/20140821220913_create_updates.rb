class CreateUpdates < ActiveRecord::Migration
  def change
    create_table :updates do |t|
      t.string :title
      t.integer :post_id

      t.timestamps
    end
  end
end
